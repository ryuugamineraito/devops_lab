variable "vm_admin" {
  description = "admin name"
}
variable "vm_password" {
    description = "admin password"
}
variable "public_key_dir" {
    description = "public key directory"
}
variable "private_key_dir" {
    description = "private key directory"
}
variable "machine_type" {
    type = list
    description = "list of machine type"
  
}