terraform {
  required_providers {
      azurerm = {
          source  = "hashicorp/azurerm"
          version = "~> 2.65"
      }

  }
  required_version = ">= 0.14.9"
}

locals {
    setup_name = "Ryuugamine"
}

provider "azurerm" {
  features {}
}
#create resource group
resource "azurerm_resource_group" "rg" {
    name = local.setup_name
    location = "westus2"
    tags = {
        Name = "Ryuugamine rg"
    }
  
}
#create vnet
resource "azurerm_virtual_network" "vnet" {
    name = "${local.setup_name}-Vnet"
    address_space = [ "10.0.0.0/16" ]
    location = azurerm_resource_group.rg.location
    resource_group_name = azurerm_resource_group.rg.name
}
#create subnet
resource "azurerm_subnet" "subnet" {
    name = "${local.setup_name}-subnet"
    resource_group_name = azurerm_resource_group.rg.name
    virtual_network_name = azurerm_virtual_network.vnet.name
    address_prefixes = [ "10.0.1.0/24" ]
}

resource "azurerm_network_security_group" "nsg" {
  name                = "${local.setup_name}_nsg"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  security_rule {
    name                       = "windown-rule"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_ranges    = [5985, 22, 3389]
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "Allow-ping-rule"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Icmp"
    source_port_range          = "*"
    destination_port_range     = "*"
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }

}

# Associate NSG with Subnet so systems are protected
resource "azurerm_subnet_network_security_group_association" "sg_assoc" {
  subnet_id                 = azurerm_subnet.subnet.id
  network_security_group_id = azurerm_network_security_group.nsg.id
}

#create public ip to assign to VM
resource "azurerm_public_ip" "public_ip" {
    for_each = toset(var.machine_type)
    name                         = "${each.key}-public-ip"
    location                     = azurerm_resource_group.rg.location
    resource_group_name          = azurerm_resource_group.rg.name
    allocation_method            = "Dynamic"

    tags = {
        environment = "${local.setup_name} Demo"
    }
}

#create network interface for vm
resource "azurerm_network_interface" "nic" {
    for_each = toset(var.machine_type)
    name = "${each.key}-nic"
    location = azurerm_resource_group.rg.location
    resource_group_name = azurerm_resource_group.rg.name

    ip_configuration {
      name = "internal"
      subnet_id = azurerm_subnet.subnet.id
      private_ip_address_allocation = "Dynamic"
      public_ip_address_id = azurerm_public_ip.public_ip[each.key].id
    }
}



# resource "azurerm_virtual_machine" "vms" {
#     name = "${local.setup_name}-vm"
#     location = azurerm_resource_group.rg.location
#     resource_group_name = azurerm_resource_group.rg.name
#     network_inetwork_interface_ids = [azurerm_network_interface.nic.id]  
#     vm_size = "Standard_F2"

#     delete_data_disks_on_termination = true
#     delete_data_disks_on_termination = true

#     storage_image_reference {
#       publisher = "Canonical"
#       offer     = "UbuntuServer"
#       sku       = "20.04-LTS"
#       version   = "latest"
#   }
#   os_profile_linux_config {
#       disable_password_authentication = true
#   }

# }

#create linux vm
resource "azurerm_linux_virtual_machine" "linux-vm"{
    name = "${local.setup_name}-linux-vm"
    location = azurerm_resource_group.rg.location
    resource_group_name = azurerm_resource_group.rg.name
    size                = "Standard_F2"
    admin_username      = var.vm_admin
    network_interface_ids = [azurerm_network_interface.nic[var.machine_type[0]].id]

    os_disk {
      caching = "ReadWrite"
      storage_account_type = "Standard_LRS"
    }
    
    admin_ssh_key {
        username = var.vm_admin
        public_key = file("${var.public_key_dir}")
    }
    source_image_reference  {
      publisher = "Canonical"
      offer     = "UbuntuServer"
      sku       = "18.04-LTS"
      version   = "latest"
    }

    # provisioner "remote-exec" {
    #     inline = [
    #       "echo 'Wait unitl SSH is ready'"
    #     ]
    #     connection {
    #         type = "ssh"
    #         user = var.vm_admin
    #         host = self.public_ip_address
    #         private_key = file("${var.private_key_dir}")  
    #     }
      
    # }
    provisioner "local-exec" {
        command = <<EOT
        sed -i '/\[linux\]/a\${self.admin_username}@${self.public_ip_address}' inventory
 
    EOT
    }
}

resource "azurerm_windows_virtual_machine" "windows-vm" {
    name                = "${local.setup_name}-windows-vm"
    resource_group_name = azurerm_resource_group.rg.name
    location            = azurerm_resource_group.rg.location
    size                = "Standard_F2"
    admin_username      = var.vm_admin
    admin_password      = var.vm_password
    computer_name = local.setup_name
    network_interface_ids = [
    azurerm_network_interface.nic[var.machine_type[1]].id,
    ]
    enable_automatic_updates = true
    custom_data = filebase64("./files/winrm.ps1")
    os_disk {
        caching              = "ReadWrite"
        storage_account_type = "Standard_LRS"
    }

    source_image_reference {
        publisher = "MicrosoftWindowsServer"
        offer     = "WindowsServer"
        sku       = "2016-Datacenter"
        version   = "latest"
    }
    winrm_listener {
        protocol = "Http"
    }
    
    additional_unattend_content {
        content = "<AutoLogon><Password><Value>${var.vm_password}</Value></Password><Enabled>true</Enabled><LogonCount>1</LogonCount><Username>${var.vm_admin}</Username></AutoLogon>"
        setting = "AutoLogon"
    }
    additional_unattend_content {
        content = file("./files/FirstLogonCommands.xml")
        setting = "FirstLogonCommands"
    }

    connection {
        host     = self.public_ip_address
        type     = "winrm"
        port     = 5985
        https    = false
        timeout  = "2m"
        user     = var.vm_admin
        password = var.vm_password
    }

    # provisioner "file" {
    #     source      = "./files/config.ps1"
    #     destination = "c:/terraform/config.ps1"
    # }

   
    provisioner "local-exec" {
        on_failure = continue
        command = <<EOT
      sed -i '/\[windows\]/a\${self.public_ip_address}' inventory
      ansible-playbook java.yml
    EOT
    }
}

# resource "azurerm_virtual_machine" "window_vm" {
#   name                = "${local.setup_name}-windows-vm"

#   resource_group_name = azurerm_resource_group.rg.name
#   location            = azurerm_resource_group.rg.location

#   network_interface_ids = [azurerm_network_interface.nic[var.machine_type[1]].id]
#   vm_size               = "Standard_F2"

#   storage_os_disk {
#     name              = "osdisk"
#     caching           = "ReadWrite"
#     create_option     = "FromImage"
#     managed_disk_type = "Standard_LRS"
#   }
#   storage_image_reference {
#     publisher = "MicrosoftWindowsServer"
#     offer     = "WindowsServer"
#     sku       = "2016-Datacenter"
#     version   = "latest"
#   }

#   os_profile {
#     computer_name  = local.setup_name
#     admin_username = var.vm_admin
#     admin_password = var.vm_password
#     custom_data    = file("./files/winrm.ps1")
#   }

#   os_profile_windows_config {
#     provision_vm_agent = true
#     winrm {
#       protocol = "http"
#     }

#     # Auto-Login's required to configure WinRM
#     additional_unattend_config {
#       pass         = "oobeSystem"
#       component    = "Microsoft-Windows-Shell-Setup"
#       setting_name = "AutoLogon"
#       content      = "<AutoLogon><Password><Value>${var.vm_password}</Value></Password><Enabled>true</Enabled><LogonCount>1</LogonCount><Username>${var.vm_admin}</Username></AutoLogon>"
#     }

#     # Unattend config is to enable basic auth in WinRM, required for the provisioner stage.
#     additional_unattend_config {
#       pass         = "oobeSystem"
#       component    = "Microsoft-Windows-Shell-Setup"
#       setting_name = "FirstLogonCommands"
#       content      = file("./files/FirstLogonCommands.xml")
#     }
#   }

#   connection {
#     host     = azurerm_public_ip.public_ip[var.machine_type[1]].id
#     type     = "winrm"
#     port     = 5985
#     https    = false
#     timeout  = "2m"
#     user     = var.vm_admin
#     password = var.vm_password

#   }

#   provisioner "file" {
#     source      = "./files/config.ps1"
#     destination = "c:/terraform/config.ps1"
#   }

#   provisioner "remote-exec" {
#     on_failure = continue
#     inline = [
#       "powershell.exe -ExecutionPolicy Bypass -File C:/terraform/config.ps1",
#     ]
#   }

#   provisioner "local-exec" {
#         command = <<EOT
#       sed -i '/\[windows\]/a\${azurerm_public_ip.public_ip[var.machine_type[1]].id}' inventory
#       ansible-playbook java.yml
#     EOT
#   }
# }



