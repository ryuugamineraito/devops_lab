{{/*
Expand the name of the chart.
*/}}
{{- define "angular-fe.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{- define "angular-fe.fullname" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}


{{/*
Create chart name and version as used by the chart label.
*/}}


{{/*
Common labels
*/}}
{{- define "angular-fe.labels" -}}
{{ include "angular-fe.selectorLabels" . }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "angular-fe.selectorLabels" -}}
app.kubernetes.io/name: {{ include "angular-fe.name" . }}
{{- end }}


