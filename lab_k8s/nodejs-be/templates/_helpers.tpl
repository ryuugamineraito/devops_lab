{{/*
Expand the name of the chart.
*/}}
{{- define "nodejs-be.name" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "nodejs-be.fullname" -}}
{{- default .Chart.Name .Values.nameOverride | trunc 63 | trimSuffix "-" }}
{{- end }}

{{/*
Create chart name and version as used by the chart label.
*/}}

{{/*
Common labels
*/}}
{{- define "nodejs-be.labels" -}}
{{ include "nodejs-be.selectorLabels" . }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "nodejs-be.selectorLabels" -}}
app.kubernetes.io/name: {{ include "nodejs-be.name" . }}
{{- end }}

